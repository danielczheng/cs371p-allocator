// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2018
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>
#include <sstream>

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        // <your code>
        int b_sentinel = 0;
        int e_sentinel = 0;

        int i = 0;
        while (i < (int) N) {
            b_sentinel = (*this)[i];
            int block_size = abs(b_sentinel);
            e_sentinel = (*this)[i + block_size + sizeof(int)];
            int prev_sentinel = 0;
            if (i != 0)
                prev_sentinel = (*this)[i - sizeof(int)];
            if (b_sentinel != e_sentinel)
                return false;
            if (i != 0 && prev_sentinel > 0 && b_sentinel > 0)
                return false;
            i += 2 * sizeof(int) + block_size;
        }
        return true;
    }

public:
    // ------------
    // constructors
    // ------------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // <your code>
        if (N < sizeof(T) + (2 * sizeof(int)))
            throw std::bad_alloc();
        int block_size = N - 2 * sizeof(int);
        // std::cout << "Block size: " << block_size << std::endl;
        (*this)[0] = block_size;                 // beginning sentinel
        (*this)[N - sizeof(int)] = block_size;   // ending    sentinel
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        if (n == 0 || n * sizeof(T) > N - 2*sizeof(int)) {
            throw std::bad_alloc();
        }
        assert(valid());

        int alloc_size  = n * sizeof(T);
        // alloc_total represents the actual bytes consumed;
        // only end sentinel is created, so we only account for one sentinel
        int alloc_total = alloc_size + sizeof(int);

        int i = 0;
        while (i < (int) N) {
            int b_sentinel = (*this)[i];
            int block_size = abs(b_sentinel);
            if (b_sentinel < 0 || block_size < alloc_size) {
                // if block is closed or too small
                i += block_size + 2 * sizeof(int);
            } else {
                if (block_size == alloc_size) {
                    // perfect fit scenario
                    (*this)[i] = -alloc_size;
                    (*this)[i + block_size + sizeof(int)] = -alloc_size;
                    assert(valid());
                    return (T*) &(*this)[i + sizeof(int)];
                } else if (block_size < alloc_total) {
                    // still too small for allocation + sentinels
                    i += block_size + 2 * sizeof(int);
                } else if (block_size > alloc_total) {
                    // block is big enough
                    int rem = block_size - alloc_total;
                    int min_block_size = sizeof(T) + (sizeof(int));
                    if (rem < min_block_size) {
                        // not enough space remaining for another block;
                        // allocate the whole block instead
                        (*this)[i] = -block_size;
                        (*this)[i + block_size + sizeof(int)] = -block_size;
                        assert(valid());
                        return (T*) &(*this)[i + sizeof(int)];
                    } else {
                        // remainder is big enough to fit more block(s)
                        (*this)[i] = -alloc_size;
                        (*this)[i + alloc_size + sizeof(int)] = -alloc_size;

                        // create remainder free block
                        int rem_block_size = rem - sizeof(int);
                        (*this)[i + alloc_size + 2*sizeof(int)] = rem_block_size;
                        (*this)[i + block_size + sizeof(int)] = rem_block_size;
                        assert(valid());
                        return (T*) &(*this)[i + sizeof(int)];
                    }
                }
            }
        }
        throw std::bad_alloc();
        return nullptr;
    }           // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     */
    void deallocate (pointer p, size_type) {
        // <your code>
        if (p == nullptr)
            throw std::invalid_argument("Pointer is invalid.");
        assert(valid());

        int i = (int) ((char*) p - a) - sizeof(int);

        int this_sentinel = (*this)[i];
        assert(this_sentinel < 0);
        int this_block_size = -this_sentinel;
        (*this)[i] = this_block_size;
        (*this)[i + this_block_size + sizeof(int)] = this_block_size;

        // coalese previous block if necessary
        int total_block_size = this_block_size;
        if (i != 0 && (*this)[i - sizeof(int)] > 0) {
            int prev_block_size = (*this)[i - sizeof(int)];
            total_block_size += prev_block_size + 2*sizeof(int);
            (*this)[i + this_block_size + sizeof(int)] = total_block_size;
            (*this)[i - prev_block_size - 2*sizeof(int)] = total_block_size;
            i = i - prev_block_size - 2*sizeof(int);
            this_block_size = total_block_size;
        }

        // coalese next block if necessary
        if (i + this_block_size + sizeof(int) != N - sizeof(int) &&
                (*this)[i + this_block_size + 2*sizeof(int)] > 0) {
            int next_block_size = (*this)[i + this_block_size + 2*sizeof(int)];
            total_block_size += next_block_size + 2*sizeof(int);
            (*this)[i] = total_block_size;
            (*this)[i + this_block_size +
                      next_block_size + 3*sizeof(int)] = total_block_size;
        }
        assert(valid());

    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    void print_sentinels(std::ostream& w) {
        int i = 0;
        while (i < (int) N) {
            int sentinel = (*this)[i];
            std::string space = " ";
            int e_sentinel_index = i + sentinel + sizeof(int);
            if (e_sentinel_index >= (int) (N - sizeof(int))) {
                space = "";
            }
            w << sentinel << space;
            i += abs(sentinel) + 2*sizeof(int);
        }
    }
};

// --------------
// allocator_eval
// --------------

/**
 * O(1) in space
 * O(n) in time
 * evaluates a given test and outputs the result
 * n is the length of the test
 */

void allocator_eval (std::istream& r, std::ostream& w) {
    const int s = 1000;
    const double v = 2.0;
    my_allocator<double, s> x;

    while (r.good()) {
        char line_buffer[5];
        r.getline(line_buffer, 5);
        std::string line(line_buffer);
        if (line == "")
            break;
        std::istringstream is(line);
        int request;
        is >> request;
        if (request >= 0) {
            double* p = x.allocate(request);
            double* e = p + request;
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        } else {
            int busy_block_id = -request;
            int cur_busy_block_id = 0;
            int i = 0;

            int sentinel = 0;
            while (cur_busy_block_id < busy_block_id && i < s) {
                sentinel = x[i];
                if (sentinel < 0) {
                    ++cur_busy_block_id;
                    if (cur_busy_block_id == busy_block_id)
                        break;
                }
                i += abs(sentinel) + 2*sizeof(int);
            }
            double* p = reinterpret_cast<double*>(&x[i+sizeof(int)]);
            double* e = p + abs(sentinel);
            while (p != e) {
                x.destroy(p);
                ++p;
            }
            x.deallocate(reinterpret_cast<double*>(&x[i+sizeof(int)]), s);
        }
    }
    x.print_sentinels(w);
}

// ---------------
// allocator_solve
// ---------------

/**
 * O(1) in space
 * O(n*t) in time
 * solves all the tests from input and outputs the end state of the sentinels
 * n is the length of the test
 * t is the number of tests given
 */

void allocator_solve (std::istream& r, std::ostream& w) {
    int num_tests;
    r >> num_tests;
    char line_buffer[80];
    r.getline(line_buffer, 80);
    r.getline(line_buffer, 80);
    for (int i = 0; i < num_tests; ++i) {
        allocator_eval(r, w);
        w << std::endl;
    }
}

#endif // Allocator_h
